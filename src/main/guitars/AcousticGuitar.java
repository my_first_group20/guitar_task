package main.guitars;

public class AcousticGuitar extends Guitar {

    public AcousticGuitar(String brand, String model, int year) {
        super(brand, model, year);
    }

    @Override
    public void makeSound() {
        System.out.println("Acoustic guitar is playing");
    }
}
