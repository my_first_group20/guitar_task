package main.guitars;

public class ElectricGuitar extends Guitar {
    String pickup;

    public ElectricGuitar(String brand, String model, int year, String pickup) {
        super(brand, model, year);
        this.pickup = pickup;
    }

    @Override
    public void makeSound() {
        System.out.println("Electric guitar is playing");
    }

    public void testCasting() {
        System.out.println("casting succesful");
    }
}
