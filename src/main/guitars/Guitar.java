package main.guitars;

import main.behaviors.Playable;

public abstract class Guitar implements Playable {
    protected String brand;
    protected String model;
    protected int year;

    public Guitar(String brand, String model, int year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }

    @Override
    public void makeSound() {}
}
