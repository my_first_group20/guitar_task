package main.behaviors;

public interface Playable {

    public void makeSound();
}
