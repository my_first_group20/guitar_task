package main;

import main.guitars.AcousticGuitar;
import main.guitars.ElectricGuitar;
import main.guitars.Guitar;

import java.util.ArrayList;

public class Program {

    public static void main(String[] args) {

        ArrayList<Guitar> myGuitars = new ArrayList<>();

        ElectricGuitar myElectricGuitar = new ElectricGuitar("Ibanez", "XPTB720", 2017, "Partsland");
        AcousticGuitar myAcousticGuitar = new AcousticGuitar("Yamaha", "APX-600", 2015);

        myGuitars.add(myElectricGuitar);
        myGuitars.add(myAcousticGuitar);

        myGuitars.get(0).makeSound();
        myGuitars.get(1).makeSound();

        //getting it out of ArrayList and casting type back to ElectricGuitar from Guitar:
        ElectricGuitar myeg = (ElectricGuitar) myGuitars.get(0);
        myeg.testCasting();

    }
}
